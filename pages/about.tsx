import StandardPage from '../components/standardPage/StandardPage';
import ThemeProvider from '../components/theme/ThemeProvider';

export default function About() {
  return (
    <ThemeProvider>
      <StandardPage>
        <p>This is the about page</p>
      </StandardPage>
    </ThemeProvider>
  );
}