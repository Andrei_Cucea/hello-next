import StandardPage from '../components/standardPage/StandardPage';
import ThemeProvider from '../components/theme/ThemeProvider';
import Timer from '../components/timer/Timer'

function Home() {
  return (
    <ThemeProvider>
      <StandardPage>
        <Timer />
      </StandardPage>
    </ThemeProvider>
  );
}

export default Home;