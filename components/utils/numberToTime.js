export function numberToTime(number, isSec) {
  var ms = isSec ? number * 1e3 : number,
    lm = ~(4 * !!isSec),  /* limit fraction */
    fmt = new Date(ms).toISOString().slice(11, lm);

  if (ms >= 8.64e7) {  /* >= 24 hours */
    var parts = fmt.split(/:(?=\d{2}:)/);
    parts[0] -= -24 * (ms / 8.64e7 | 0);
    return parts.join(':');
  }

  return fmt;
};