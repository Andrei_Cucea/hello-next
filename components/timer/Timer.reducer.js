import React from 'react';
import { formatDistanceStrict, addMilliseconds, fromUnixTime, getUnixTime } from 'date-fns';
import { numberToTime } from '../utils/numberToTime';
import { getTime } from 'date-fns';

export const initialState = {
  startTime: new Date(),
  endTime: new Date(),
  remainingTime: 0,
  interval: 0,
  running: false,
  repeat: false,
};

const TimerContext = React.createContext();

function timerReducer(state, action) {
  switch (action.type) {
    case 'addMinute': {
      return {
        ...state,
        interval: state.interval + 60000,
      };
    }
    case 'removeMinute': {
      return {
        ...state,
        interval: state.interval - 60000,
      };
    }
    case 'addSecond': {
      return {
        ...state,
        interval: state.interval + 1000,
      };
    }
    case 'removeSecond': {
      return {
        ...state,
        interval: state.interval - 1000,
      };
    }
    case 'setRemainingTime': {
      let remainingTime = getTime(state.endTime) - getTime(new Date());

      remainingTime = remainingTime < 0 ? 0 : remainingTime;

      if (state.repeat && remainingTime === 0) {
        remainingTime = state.interval;
      }

      return {
        ...state,
        remainingTime: remainingTime,
      };
    }
    case 'start': {
      const startTime = new Date();
      const endTime = fromUnixTime(getUnixTime(new Date()) + parseInt((state.interval/1000).toFixed()));
      const remainingTime = getTime(endTime) - getTime(startTime);

      return {
        ...state,
        running: true,
        startTime: startTime,
        endTime: endTime,
        remainingTime: remainingTime,
      }
    }
    case 'stop': {
      return {
        ...state,
        running: false,
        startTime: new Date(),
        endTime: new Date(),
        remainingTime: 0
      }
    }
    case 'pause': {
      return {
        ...state,
        running: state.running ? !state.running : state.running,
      }
    }
    case 'toggleRepeat': {
      return {
        ...state,
        repeat: !state.repeat,
      }
    }
    default:
      throw new Error();
  }
}

function TimerProvider(props) {
  const [state, dispatch] = React.useReducer(timerReducer, initialState);
  const value = React.useMemo(() => [state, dispatch], [state]);

  return <TimerContext.Provider value={value} {...props} />
}

function useTimer() {
  const context = React.useContext(TimerContext)
  if (!context) {
    throw new Error(`useCount must be used within a CountProvider`)
  }
  return context
}

export { TimerProvider, useTimer }