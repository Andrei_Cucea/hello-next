import { useReducer, useEffect } from 'react';
import { initialState, reducer } from './Timer.reducer';
import { numberToTime } from '../utils/numberToTime';
import Checkbox from '@material-ui/core/Checkbox';

export default function About() {
  const [state, dispatch] = useReducer(reducer, initialState);
  const { interval, remainingTime, running, repeat } = state;

  useEffect(() => {
    if (running) {
      setTimeout(() => {
        dispatch({ type: 'setRemainingTime' });
        console.log(remainingTime);
      }, 66);
    }
  }, [running, remainingTime]);

  return (
    <>
      <div>
        <button onClick={() => dispatch({ type: 'addMinute' })}>
          Add minute
        </button>
        <button onClick={() => dispatch({ type: 'addSecond' })}>
          Add second
        </button>
        <div>Interval: {interval}</div>
        <button onClick={() => dispatch({ type: 'removeMinute' })}>
          Remove minute
        </button>
        <button onClick={() => dispatch({ type: 'removeSecond' })}>
          Remove second
        </button>
      </div>

      <div>{numberToTime(remainingTime)}</div>
      <div>Remaining Time: {remainingTime}</div>

      <div>
        <button onClick={() => dispatch({ type: 'start' })}>
          {running ? 'Restart' : 'Start'}
        </button>
        <button onClick={() => dispatch({ type: 'pause' })}>
          Pause
        </button>
        <button onClick={() => dispatch({ type: 'stop' })}>
          Stop
        </button>
        <Checkbox checked={repeat} onChange={() => dispatch({type: 'toggleRepeat'})}></Checkbox>
      </div>

    </>
  );
}