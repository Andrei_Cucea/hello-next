import NavBar from '../navbar/NavBar';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import Paper from '@material-ui/core/Paper';

export default function StandardPage(props) {
  return (
    <>
      <NavBar />
      <Container maxWidth={'sm'}>
        <Paper>
          {props.children}
        </Paper>
      </Container>
    </>
  );
}